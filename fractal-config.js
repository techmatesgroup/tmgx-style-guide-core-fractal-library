'use strict';

/*
* Require the path module
*/
const path = require('path');
const pkg = require(path.join(__dirname, 'package.json'));

/*
 * Require the Fractal module
 */
const fractalConfig = module.exports = require('@frctl/fractal').create();

fractalConfig.set('project.title', 'Components library');
fractalConfig.set('project.version', pkg.version);
fractalConfig.set('project.author', 'TMG');

fractalConfig.components.set('ext', '.html');

fractalConfig.components.set('path', path.join(__dirname, 'components'));

fractalConfig.docs.set('path', path.join(__dirname, 'docs'));

fractalConfig.web.set('static.path', path.join(__dirname, 'public'));
fractalConfig.web.set('server.sync', true);
fractalConfig.web.set('server.port', 5000);
fractalConfig.web.set('builder.dest', path.join(__dirname, 'dist'));

const defaultTheme = require('./themes/default');
fractalConfig.web.theme(defaultTheme);